TEMPLATE = lib

CONFIG	+= staticlib x86 debug
#CONFIG	+= staticlib x86 release

win32{

HEADERS = \
    Screen/capturer.h \
    Screen/screencapturerbase_win.h

SOURCES = \
    Screen/capturer.cpp \
    Screen/screencapturerbase_win.cpp

DEFINES =  PLATFORM_WIN32

}
