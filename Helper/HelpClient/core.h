
/*****************************************************************************
FILE		: core.h
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef __CORE_H__
#define __CORE_H__

#include "mainwindow.h"

#include <QObject>
#include <QtGui/QApplication>

////////////////////////////////////////////////////////////////////////////////

#define	g_Core	Core::getInstance()

////////////////////////////////////////////////////////////////////////////////

class	SessionThread;

////////////////////////////////////////////////////////////////////////////////

class Core
{
public:
    Core( int argc, char *argv[] );
    ~Core();

    static  Core*    getInstance() { return m_instance; }

    int	    exec();
    void    initialize();
    void    startSession( bool isHost );

    MainWindow*		getMainWindow();
    SessionThread*	getSessionThread() const;

private:
    static   Core*   m_instance;

    MainWindow*	    m_mainWindow;
    QApplication*   m_application;
    SessionThread*  m_sessionThread;
};
////////////////////////////////////////////////////////////////////////////////

inline
MainWindow* Core::getMainWindow()
{
    return  m_mainWindow;
}
////////////////////////////////////////////////////////////////////////////////

inline
SessionThread* Core::getSessionThread()   const
{
    return  m_sessionThread;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __CORE_H__