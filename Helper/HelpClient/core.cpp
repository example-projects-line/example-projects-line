
/*****************************************************************************
FILE		: core.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "core.h"
#include "session/statemachine/sessionthread.h"

////////////////////////////////////////////////////////////////////////////////

Core*	Core::m_instance  = NULL;

////////////////////////////////////////////////////////////////////////////////

Core::Core( int argc, char *argv[] ) : m_sessionThread( NULL )
{
    m_application   = new QApplication( argc, &*argv );

    if( m_instance != NULL )
	return;

    m_instance	    = this;
    m_mainWindow    = new MainWindow();
}
////////////////////////////////////////////////////////////////////////////////

Core::~Core()
{
}
////////////////////////////////////////////////////////////////////////////////

int Core::exec()
{
    return  m_application->exec();
}
////////////////////////////////////////////////////////////////////////////////

void	Core::initialize()
{
    m_mainWindow->show();
}
////////////////////////////////////////////////////////////////////////////////

void	Core::startSession( bool isHost )
{
    if( m_sessionThread == NULL )
    {
	m_sessionThread   = new SessionThread( isHost );
	m_sessionThread->start();
    }
}
////////////////////////////////////////////////////////////////////////////////