/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri 15. Jun 01:41:32 2012
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *tabJoinSession;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *lblIpAddress;
    QLineEdit *txtIpAddress;
    QPushButton *btnJoinSession;
    QWidget *tabStartSession;
    QPushButton *btnStartSession;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(310, 261);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 311, 241));
        tabJoinSession = new QWidget();
        tabJoinSession->setObjectName(QString::fromUtf8("tabJoinSession"));
        layoutWidget = new QWidget(tabJoinSession);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 0, 216, 70));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        lblIpAddress = new QLabel(layoutWidget);
        lblIpAddress->setObjectName(QString::fromUtf8("lblIpAddress"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, lblIpAddress);

        txtIpAddress = new QLineEdit(layoutWidget);
        txtIpAddress->setObjectName(QString::fromUtf8("txtIpAddress"));

        formLayout->setWidget(1, QFormLayout::LabelRole, txtIpAddress);

        btnJoinSession = new QPushButton(layoutWidget);
        btnJoinSession->setObjectName(QString::fromUtf8("btnJoinSession"));

        formLayout->setWidget(2, QFormLayout::FieldRole, btnJoinSession);

        tabWidget->addTab(tabJoinSession, QString());
        tabStartSession = new QWidget();
        tabStartSession->setObjectName(QString::fromUtf8("tabStartSession"));
        btnStartSession = new QPushButton(tabStartSession);
        btnStartSession->setObjectName(QString::fromUtf8("btnStartSession"));
        btnStartSession->setGeometry(QRect(200, 180, 100, 25));
        tabWidget->addTab(tabStartSession, QString());
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        lblIpAddress->setText(QApplication::translate("MainWindow", "IP address:", 0, QApplication::UnicodeUTF8));
        btnJoinSession->setText(QApplication::translate("MainWindow", "Join session", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabJoinSession), QApplication::translate("MainWindow", "Join session", 0, QApplication::UnicodeUTF8));
        btnStartSession->setText(QApplication::translate("MainWindow", "Start session", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabStartSession), QApplication::translate("MainWindow", "Start session", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
