
/*****************************************************************************
FILE		: viewerdialog.cpp
DESCRIPTION	: Session viewer dialog.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "viewerdialog.h"
#include "ui_viewerdialog.h"

#include "core.h"
#include "session/session.h"
#include "session/statemachine/sessionthread.h"

#include <QMutex>
#include <QTimer>
#include <QMutexLocker>

////////////////////////////////////////////////////////////////////////////////

#define	SYNC	 QMutexLocker	locker( m_mutex );

////////////////////////////////////////////////////////////////////////////////

ViewerDialog::ViewerDialog( QWidget* parent ) : QFrame( parent ),
    m_ui( new Ui::ViewerDialog ),
    m_mutex( NULL ),
    m_updateScreenTimer( NULL )
{
    m_ui->setupUi(this);

    m_mutex		= new QMutex();
    m_updateScreenTimer	= new QTimer();

    connect( m_updateScreenTimer, SIGNAL(timeout()), SLOT(onUpdateScreenTimeout()) );
    m_updateScreenTimer->start( 33 );
}
////////////////////////////////////////////////////////////////////////////////

ViewerDialog::~ViewerDialog()
{
    if( m_mutex != NULL )
    {
	delete	m_mutex;
	m_mutex	= NULL;
    }
    if( m_updateScreenTimer != NULL )
    {
	delete m_updateScreenTimer;
	m_updateScreenTimer = NULL;
    }

    delete m_ui;
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::updateViewerScene( const QImage& screen )
{
    SYNC;
    QPixmap pixmap  = QPixmap::fromImage( screen );
    m_ui->lblScreenImage->resize( pixmap.size() );
    m_ui->lblScreenImage->setPixmap( pixmap );
    m_ui->lblScreenImage->move( QPoint(0, 0) );
}
////////////////////////////////////////////////////////////////////////////////

void	ViewerDialog::onUpdateScreenTimeout()
{
    SessionThread* thread = g_Core->getSessionThread();

    if( thread != NULL )
    {
	Session*	    session = thread->getSession();

	if( session != NULL )
	{
	    QImage	screen	= session->screen();

	    updateViewerScene( screen );
	}
    }
}
////////////////////////////////////////////////////////////////////////////////