/********************************************************************************
** Form generated from reading UI file 'viewerdialog.ui'
**
** Created: Fri 15. Jun 01:41:32 2012
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWERDIALOG_H
#define UI_VIEWERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_ViewerDialog
{
public:
    QLabel *lblScreenImage;

    void setupUi(QFrame *ViewerDialog)
    {
        if (ViewerDialog->objectName().isEmpty())
            ViewerDialog->setObjectName(QString::fromUtf8("ViewerDialog"));
        ViewerDialog->resize(400, 300);
        ViewerDialog->setFrameShape(QFrame::StyledPanel);
        ViewerDialog->setFrameShadow(QFrame::Raised);
        lblScreenImage = new QLabel(ViewerDialog);
        lblScreenImage->setObjectName(QString::fromUtf8("lblScreenImage"));
        lblScreenImage->setGeometry(QRect(0, 10, 401, 281));

        retranslateUi(ViewerDialog);

        QMetaObject::connectSlotsByName(ViewerDialog);
    } // setupUi

    void retranslateUi(QFrame *ViewerDialog)
    {
        ViewerDialog->setWindowTitle(QApplication::translate("ViewerDialog", "Frame", 0, QApplication::UnicodeUTF8));
        lblScreenImage->setText(QApplication::translate("ViewerDialog", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ViewerDialog: public Ui_ViewerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWERDIALOG_H
