
/*****************************************************************************
FILE		: Session.h
DESCRIPTION	: Session thread class

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef __SESSION_H__
#define __SESSION_H__

////////////////////////////////////////////////////////////////////////////////

#include <QMutex>
#include <QImage>
#include <QObject>

////////////////////////////////////////////////////////////////////////////////

class	QTcpServer;
class	QTcpSocket;
class	QMutexLocker;
class	SessionThread;

////////////////////////////////////////////////////////////////////////////////

class Session
{

public:
    Session();
    ~Session();

    void    setPresenter( bool present );
    void    setScreen( const QImage& img );
    void    initializeSocketAndServer();

    bool    isPresenter()	const;

    QImage  screen();

    void    onListen();
    void    onConnect();
    void    onReadyRead();
    void    onConnected();
    void    onInitialize();
    void    onNewConnection();

    bool    onWaitForRead();
    bool    onWaitForConnected();
    bool    onWaitForConnection();

private:

    int	    m_screenBytes;

    bool    m_isPresenter;

    QMutex	    m_mutex;
    QImage	    m_image;
    QByteArray	    m_screenArray;

    QTcpSocket*	    m_socket;
    QTcpServer*	    m_server;
};
////////////////////////////////////////////////////////////////////////////////

inline
bool	Session::isPresenter()  const
{
    return  m_isPresenter;
}
////////////////////////////////////////////////////////////////////////////////

inline
void	Session::setPresenter( bool present )
{
    m_isPresenter   = present;
}
////////////////////////////////////////////////////////////////////////////////

#endif // __SESSION_H__
