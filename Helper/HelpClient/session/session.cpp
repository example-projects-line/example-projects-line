
/*****************************************************************************
FILE		: Session.cpp
DESCRIPTION	: Session thread class

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

////////////////////////////////////////////////////////////////////////////////

#include "Session.h"

#include <QImage>
#include <QMutex>
#include <QBuffer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMutexLocker>

#include "../core.h"
#include "../viewerdialog.h"

#include "../globaldefinitions.h"

////////////////////////////////////////////////////////////////////////////////

#define	SYNC	 QMutexLocker	locker( &m_mutex )

////////////////////////////////////////////////////////////////////////////////

Session::Session() : m_screenBytes( -1 ),
    m_isPresenter( false ),
    m_server( NULL ),
    m_socket( NULL )
{

}
////////////////////////////////////////////////////////////////////////////////

Session::~Session()
{
    if( m_server != NULL )
    {
	delete	m_server;
	m_server    = NULL;
    }

    if( m_socket != NULL )
    {
	delete m_socket;
	m_socket    = NULL;
    }
}
////////////////////////////////////////////////////////////////////////////////

void	Session::setScreen( const QImage& img )
{
    SYNC;
    m_image = img;
//    QImage firstBox	= m_image.copy( 0, 0, 16, 16 );
//    QImage fBox	= m_image.copy( 0, 0, 16, 16 );
//    if( ! firstBox.isNull() )
//	firstBox.save( "fbox1.png", "PNG" );
//    else
//	qWarning() << "Sth went wrong!!!";

//    bool    areEqual	= memcmp( firstBox.bits(), fBox.bits(), 16*16*4 ) == 0 ? true : false;
//    qWarning() << "THE 2 16x16 IMAGES ARE EQUAL : " << areEqual;
}
////////////////////////////////////////////////////////////////////////////////

void	Session::initializeSocketAndServer()
{
    if( ! m_isPresenter && m_socket == NULL )
	m_socket	= new QTcpSocket();

    if( m_isPresenter && m_server == NULL )
	m_server	= new QTcpServer();
}
////////////////////////////////////////////////////////////////////////////////

QImage	Session::screen()
{
    SYNC;
    return  m_image;
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onListen()
{
    if( m_server != NULL )
    {
	qint16	    port    = TCP_LOCAL_PORT;

	m_server->listen( QHostAddress("127.0.0.1"), port );

	if( m_server->isListening() )
	{
	    qDebug() << "Started listening on port : " << m_server->serverPort();
	}
    }
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onConnect()
{
    if( m_socket != NULL && m_socket->state() == QAbstractSocket::UnconnectedState )
    {
	m_socket->connectToHost( "127.0.0.1", TCP_LOCAL_PORT, QIODevice::ReadWrite );
    }
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onReadyRead()
{
    if( m_isPresenter )
    {
	//	Read packet header.
	QDataStream	stream( m_socket );
	stream.setVersion( QDataStream::Qt_4_0 );

	int command;
	stream >> command;
	qDebug() << "Command is : " << command;

	QByteArray	imgBits;
	QBuffer	buffer( &imgBits );
	buffer.open( QIODevice::WriteOnly );

	SYNC;
	m_image.save( &buffer, "PNG" );
	buffer.close();

	qDebug() << "Send image size : " << imgBits.size();

	QDataStream out( m_socket );
	out.setVersion( QDataStream::Qt_4_0 );
	out << imgBits.size();

	m_socket->write( imgBits );
    }
    else
    {
	QDataStream	stream( m_socket );
	stream.setVersion( QDataStream::Qt_4_0 );

	//	Check for new screen update.
	if( m_screenBytes == -1 )
	{
	    stream >> m_screenBytes;

	    qDebug() << "Total screen size in bytes is : " << m_screenBytes;
	}
	else
	{
	    m_screenArray.append( m_socket->readAll() );
	    qWarning() << "ScreenArray size is : " << m_screenArray.size();

	    if( m_screenArray.size() == m_screenBytes )
	    {
		QImage	img( QSize(1200,800), QImage::Format_ARGB32 );
		img.loadFromData( m_screenArray, "PNG" );

		//	Clear buffer and reset bytes count.
		m_screenBytes = -1;
		m_screenArray.clear();

		//	Update screen image.
		setScreen( img );

		//	Return command "GET UPDATES" to host.
		QDataStream out( m_socket );
		out.setVersion( QDataStream::Qt_4_0 );
		out << COMMAND_GET_UPDATES;
	    }
	}
    }
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onConnected()
{
    if( m_socket != NULL )
    {
	QDataStream out( m_socket );
	out.setVersion( QDataStream::Qt_4_0 );

	//	Write(give) command to the host to start sending his/her screen.
	out << COMMAND_START_SENDING;
    }
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onInitialize()
{
    initializeSocketAndServer();
}
////////////////////////////////////////////////////////////////////////////////

bool	Session::onWaitForRead()
{
    if( m_socket != NULL )
	return m_socket->waitForReadyRead( 33 );

    return false;
}
////////////////////////////////////////////////////////////////////////////////

void	Session::onNewConnection()
{
    m_socket	= m_server->nextPendingConnection();

    qDebug() << "New peer trying to join the session : ";
    qDebug() << "Peer IP : " << m_socket->peerAddress();
    qDebug() << "Peer port : " << m_socket->peerPort();
}
////////////////////////////////////////////////////////////////////////////////

bool	Session::onWaitForConnected()
{
    if( m_socket != NULL )
	return m_socket->waitForConnected( 33 );

    return false;
}
////////////////////////////////////////////////////////////////////////////////

bool	Session::onWaitForConnection()
{
    if( m_server != NULL )
	return m_server->waitForNewConnection();

    return false;
}
////////////////////////////////////////////////////////////////////////////////