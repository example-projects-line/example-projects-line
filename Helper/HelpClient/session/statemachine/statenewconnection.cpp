
/*****************************************************************************
FILE		: statenewconnection.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "statenewconnection.h"

#include "../session.h"
#include "../../core.h"
#include "sessionthread.h"
#include "statewaitforread.h"

////////////////////////////////////////////////////////////////////////////////

StateNewConnection* StateNewConnection::m_instance  = NULL;

////////////////////////////////////////////////////////////////////////////////

StateNewConnection::StateNewConnection()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
StateNewConnection* StateNewConnection::instance()
{
    if( m_instance == NULL )
	m_instance  = new StateNewConnection();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	StateNewConnection::handle()
{
    SessionThread*  thread  = g_Core->getSessionThread();
    Session*	    session = thread->getSession();

    session->onNewConnection();

    thread->changeState( StateWaitForRead::instance() );
}
////////////////////////////////////////////////////////////////////////////////