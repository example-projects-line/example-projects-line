
/*****************************************************************************
FILE		: stateconnected.cpp
DESCRIPTION	: State class used when peer is connected to the host.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "stateconnected.h"

#include "../session.h"
#include "../../core.h"
#include "sessionthread.h"
#include "statewaitforread.h"

////////////////////////////////////////////////////////////////////////////////

StateConnected*    StateConnected::m_instance	= NULL;

////////////////////////////////////////////////////////////////////////////////

StateConnected::StateConnected()
{
}
////////////////////////////////////////////////////////////////////////////////

//  static
StateConnected*	StateConnected::instance()
{
    if( m_instance == NULL )
	m_instance  = new StateConnected();

    return  m_instance;
}
////////////////////////////////////////////////////////////////////////////////

void	StateConnected::handle()
{
    SessionThread*  thread  = g_Core->getSessionThread();
    Session*	    session = thread->getSession();

    session->onConnected();

    thread->changeState( StateWaitForRead::instance() );
}
////////////////////////////////////////////////////////////////////////////////