
/*****************************************************************************
FILE		: viewerdialog.h
DESCRIPTION	: Session viewer dialog.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef __VIEWERDIALOG_H__
#define __VIEWERDIALOG_H__

////////////////////////////////////////////////////////////////////////////////

#include <QFrame>

////////////////////////////////////////////////////////////////////////////////

class	QMutex;
class	QTimer;
class	QMutexLocker;

////////////////////////////////////////////////////////////////////////////////

namespace Ui
{
    class ViewerDialog;
}
////////////////////////////////////////////////////////////////////////////////

class ViewerDialog : public QFrame
{
    Q_OBJECT
    
public:
    explicit ViewerDialog( QWidget* parent = 0 );
    ~ViewerDialog();

private slots:

    void    onUpdateScreenTimeout();

private:

    void    updateViewerScene( const QImage& screen );

    Ui::ViewerDialog*	m_ui;

    QMutex*	m_mutex;
    QPixmap	m_screen;
    QTimer*	m_updateScreenTimer;
};
////////////////////////////////////////////////////////////////////////////////

#endif // __VIEWERDIALOG_H__
