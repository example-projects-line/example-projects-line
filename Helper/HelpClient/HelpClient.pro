#-------------------------------------------------
#
# Project created by QtCreator 2012-02-04T11:30:36
#
#-------------------------------------------------

QT       += core gui
QT       += network

TARGET = HelpClient
TEMPLATE = app

#CONFIG += console

SOURCES += main.cpp\
        mainwindow.cpp \
    capture/capturethread.cpp \
    core.cpp \
    viewerdialog.cpp \
    session/statemachine/stateconnected.cpp \
    session/statemachine/stateinitialize.cpp \
    session/session.cpp \
    session/statemachine/statelisten.cpp \
    session/statemachine/stateconnect.cpp \
    session/statemachine/statewaitforconnected.cpp \
    session/statemachine/statewaitforread.cpp \
    session/statemachine/stateread.cpp \
    session/statemachine/statewaitforconnection.cpp \
    session/statemachine/statenewconnection.cpp \
    session/statemachine/sessionthread.cpp

HEADERS  += mainwindow.h \
    capture/capturethread.h \
    core.h \
    viewerdialog.h \
    globaldefinitions.h \
    session/statemachine/sessionstate.h \
    session/statemachine/stateconnected.h \
    session/statemachine/stateinitialize.h \
    session/session.h \
    session/statemachine/statelisten.h \
    session/statemachine/stateconnect.h \
    session/statemachine/statewaitforconnected.h \
    session/statemachine/statewaitforread.h \
    session/statemachine/stateread.h \
    session/statemachine/statewaitforconnection.h \
    session/statemachine/statenewconnection.h \
    session/statemachine/sessionthread.h

FORMS    += mainwindow.ui \
    viewerdialog.ui

win32{
LIBS	= ../utilities/debug/utilities.lib
}
