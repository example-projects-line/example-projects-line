
/*****************************************************************************
FILE		: mainwindow.cpp
DESCRIPTION	: Application mainwindow

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "core.h"
#include "viewerdialog.h"
#include "session/session.h"
#include "capture/capturethread.h"
#include "session/statemachine/sessionthread.h"

#include <QTimer>
#include <QImage>
#include <QDesktopWidget>
#include <QDebug>

////////////////////////////////////////////////////////////////////////////////

MainWindow::MainWindow( QWidget* parent ) : QMainWindow( parent ),
    m_ui( new Ui::MainWindow ),
    m_viewerDlg( NULL ),
    m_captureThread( NULL ),
    m_scrnCaptureTimer( NULL )
{
    m_ui->setupUi( this );

    m_viewerDlg	= new ViewerDialog();

    //	Connect signals with slots.
    connect( m_ui->btnStartSession, SIGNAL(clicked()), this, SLOT(onStartSession()) );
    connect( m_ui->btnJoinSession,  SIGNAL(clicked()), this, SLOT(onJoinSession())  );
}
////////////////////////////////////////////////////////////////////////////////

MainWindow::~MainWindow()
{
    if( m_captureThread != NULL )
    {
	delete	m_captureThread;
	m_captureThread	= NULL;
    }
    if( m_session != NULL )
    {
	delete  m_session;
	m_session   = NULL;
    }
    if( m_viewerDlg !=  NULL )
    {
	delete  m_viewerDlg;
	m_viewerDlg = NULL;
    }

    delete  m_ui;
}
////////////////////////////////////////////////////////////////////////////////

void MainWindow::onStartSession()
{
    m_scrnCaptureTimer	= new QTimer( this );
    m_captureThread	= new CaptureThread();

    connect( m_scrnCaptureTimer, SIGNAL(timeout()), SLOT(onScrnTimerTimeout()), Qt::QueuedConnection );

    QDesktopWidget*  desktop = qApp->desktop();
    int	    width   = desktop->width();
    int	    height  = desktop->height();


    m_image = QImage( width, height, QImage::Format_ARGB32 );

    m_captureThread->init( width, height );
    m_captureThread->start();

    m_scrnCaptureTimer->setInterval( 300 );
    m_scrnCaptureTimer->start();
    g_Core->startSession( true );
//    m_session->setPresenter( true );
//    m_session->start();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onJoinSession()
{
    g_Core->startSession( false );
//    m_session->start();
    m_viewerDlg->show();
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onScrnTimerTimeout()
{
    m_captureThread->captureScreen( (char*)m_image.bits() );
    g_Core->getSessionThread()->getSession()->setScreen( m_image );
}
////////////////////////////////////////////////////////////////////////////////
