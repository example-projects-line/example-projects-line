
/*****************************************************************************
FILE		: mainwindow.h
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__

////////////////////////////////////////////////////////////////////////////////

#include <QMainWindow>

////////////////////////////////////////////////////////////////////////////////

class	QTimer;
class	ViewerDialog;
class	Session;
class	CaptureThread;

////////////////////////////////////////////////////////////////////////////////

namespace Ui
{
    class MainWindow;
}
////////////////////////////////////////////////////////////////////////////////

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow( QWidget* parent = 0 );
    ~MainWindow();

    Session*   getSession();

private	slots:

    void    onScrnTimerTimeout();
    void    onStartSession();
    void    onJoinSession();

private:

    QImage	    m_image;
    QTimer*	    m_scrnCaptureTimer;

    ViewerDialog*   m_viewerDlg;
    Session*  m_session;
    CaptureThread*  m_captureThread;

    Ui::MainWindow* m_ui;
};
////////////////////////////////////////////////////////////////////////////////

inline
Session*	MainWindow::getSession()
{
    return  m_session;
}
////////////////////////////////////////////////////////////////////////////////

#endif // MAINWINDOW_H
