
/*****************************************************************************
FILE		: capturethread.cpp
DESCRIPTION	: Class implementing the capture thread.

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "capturethread.h"

////////////////////////////////////////////////////////////////////////////////

#define	SYNC	 QMutexLocker	locker( &m_mutex )

////////////////////////////////////////////////////////////////////////////////

CaptureThread::CaptureThread() : QThread(),
    m_buffer( NULL ),
    m_capturer( NULL )
{
    m_capturer	= new Capturer();
}
////////////////////////////////////////////////////////////////////////////////

CaptureThread::~CaptureThread()
{
    if( m_capturer != NULL )
    {
	delete	m_capturer;
	m_capturer  = NULL;
    }
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::run()
{
    while( isRunning() )
    {
	{
	    //QMutexLocker	locker( &m_mutex );
	    SYNC;
	    m_capturer->captureDesktop( m_buffer );
	}
	msleep( 33 );
    }
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::init( int screenW, int screenH )
{
   m_buffer = (char*)realloc( m_buffer, screenW*screenH*4 );

   m_width  = screenW;
   m_height = screenH;
}
////////////////////////////////////////////////////////////////////////////////

void	CaptureThread::captureScreen( char* buffer )
{
    {
	//QMutexLocker    locker( &m_mutex );
	SYNC;
	memcpy( buffer, m_buffer, m_width*m_height*4 );
    }
}
////////////////////////////////////////////////////////////////////////////////