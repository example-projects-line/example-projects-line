
/*****************************************************************************
FILE		: mainwindow.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>

MainWindow::MainWindow( QWidget *parent ) :
    QMainWindow( parent ),
    m_socket( NULL ),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_socket	= new QTcpSocket( this );
    connect( m_socket, SIGNAL(connected()), SLOT(onConnected()) );
}
////////////////////////////////////////////////////////////////////////////////

MainWindow::~MainWindow()
{
    delete ui;
}
////////////////////////////////////////////////////////////////////////////////

void MainWindow::on_btnStart_clicked()
{
    m_socket->connectToHost( "127.0.0.1", 5858, QIODevice::ReadWrite );
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onConnected()
{
    QFile	imgFile( "C:\\Users\\SG\\Desktop\\1.jpg" );
    imgFile.open( QIODevice::ReadOnly );

    QByteArray	imgBits	= imgFile.readAll();
    QDataStream out( m_socket );
    out.setVersion( QDataStream::Qt_4_0 );
    out << imgBits.size();

    m_socket->write( imgBits );

    m_socket->disconnectFromHost();
}
////////////////////////////////////////////////////////////////////////////////
