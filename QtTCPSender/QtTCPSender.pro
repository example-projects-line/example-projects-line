#-------------------------------------------------
#
# Project created by QtCreator 2012-02-15T19:32:14
#
#-------------------------------------------------

QT       += core gui

TARGET = QtTCPSender
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

QT += network
