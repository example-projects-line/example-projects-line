#-------------------------------------------------
#
# Project created by QtCreator 2012-02-13T19:16:22
#
#-------------------------------------------------

QT       += core gui

TARGET = QtSingleton
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    core.cpp

HEADERS  += mainwindow.h \
    core.h

FORMS    += mainwindow.ui
