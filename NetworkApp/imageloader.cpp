
/*****************************************************************************
FILE		: imageloader.cpp
DESCRIPTION	: Loads a given image and returns its pixels bytes.

*****************************************************************************

Copyright (C) 2011 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "imageloader.h"

////////////////////////////////////////////////////////////////////////////////

ImageLoader::ImageLoader()
{
}
////////////////////////////////////////////////////////////////////////////////

ImageLoader::~ImageLoader()
{
}
////////////////////////////////////////////////////////////////////////////////

bool	ImageLoader::load( const QString& path )
{
    bool    isLoaded	= m_image.load( path );

    return isLoaded;
}
////////////////////////////////////////////////////////////////////////////////
