#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Windows.h"
////////////////////////////////////////////////////////////////////////////////

MainWindow::MainWindow( QWidget *parent ) :
    QMainWindow( parent ),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    CaptureScreen();
}
////////////////////////////////////////////////////////////////////////////////

MainWindow::~MainWindow()
{
    delete ui;
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::CaptureScreen()
{
    /*int nScreenWidth	= ::GetSystemMetrics( SM_CXSCREEN );
    int nScreenHeight	= ::GetSystemMetrics( SM_CYSCREEN );
    HWND hDesktopWnd	= ::GetDesktopWindow();
    int	error		= ::GetLastError();
    HDC hDesktopDC	= ::GetDC( hDesktopWnd );
    error		= ::GetLastError();
    HDC hCaptureDC	= ::CreateCompatibleDC( hDesktopDC );
    error		= ::GetLastError();
    HBITMAP hBitmap	= ::CreateCompatibleBitmap( hDesktopDC, nScreenWidth, nScreenHeight );
    error		= ::GetLastError();
    ::SelectObject( hCaptureDC, hBitmap );
    error		= ::GetLastError();
    ::BitBlt( hCaptureDC, 0, 0, nScreenWidth, nScreenHeight, hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT );
    error		= ::GetLastError();

    QPixmap pixmap;
    pixmap.fromWinHBITMAP( hBitmap, QPixmap::PremultipliedAlpha );
    pixmap.save( "desktop.bmp", "BMP" );
    ::ReleaseDC( hDesktopWnd,hDesktopDC );
    ::DeleteDC( hCaptureDC );
    ::DeleteObject( hBitmap );*/
    int nScreenWidth	= ::GetSystemMetrics(SM_CXSCREEN);
   int nScreenHeight	= ::GetSystemMetrics(SM_CYSCREEN);

   HWND hDesktopWnd	= ::GetDesktopWindow();

   HDC hDesktopDC	= ::GetDC(hDesktopWnd);
   HDC hCaptureDC	= ::CreateCompatibleDC(hDesktopDC);

   HBITMAP hCaptureBitmap = ::CreateCompatibleBitmap(hDesktopDC,nScreenWidth, nScreenHeight);

   ::SelectObject(hCaptureDC,hCaptureBitmap);

   bool	isCaptured  = ::BitBlt(hCaptureDC,0,0,nScreenWidth,nScreenHeight, hDesktopDC,0,0,SRCCOPY|CAPTUREBLT);
   // Get the BITMAP from the HBITMAP
   BITMAP   bmpScreen;
   ::GetObject( hCaptureBitmap,sizeof(BITMAP),&bmpScreen);

   BITMAPFILEHEADER   bmfHeader;
   BITMAPINFOHEADER   bi;

   bi.biSize = sizeof(BITMAPINFOHEADER);
   bi.biWidth = bmpScreen.bmWidth;
   bi.biHeight = -bmpScreen.bmHeight;  // This is upside-down image, negative will make ::GetDibBits to flip it.
   bi.biPlanes = 1;
   bi.biBitCount = 32;
   bi.biCompression = BI_RGB;
   bi.biSizeImage = 0;
   bi.biXPelsPerMeter = 0;
   bi.biYPelsPerMeter = 0;
   bi.biClrUsed = 0;
   bi.biClrImportant = 0;

   QImage   img( 1280, 800, QImage::Format_ARGB32 );
   char* buffer = (char*)img.bits();

   // Gets the "bits" from the bitmap and copies them into a buffer
   ::GetDIBits(hDesktopDC, hCaptureBitmap, 0,
       (UINT)bmpScreen.bmHeight,
       buffer,
       (BITMAPINFO *)&bi, DIB_RGB_COLORS);
//    QByteArray	data;
//    data.resize( 1280*800*4 );
//    data.fromRawData( buffer, data.size() );

   img.loadFromData( (unsigned char*)buffer, QImage::Format_ARGB32_Premultiplied );
   img.save( "screen.png", "PNG" );

   QPixmap pixmap   = QPixmap::fromWinHBITMAP( hCaptureBitmap, QPixmap::PremultipliedAlpha );
   pixmap.save( "desktop.png", "PNG" );

   ::ReleaseDC(hDesktopWnd,hDesktopDC);
   ::DeleteDC(hCaptureDC);

}
////////////////////////////////////////////////////////////////////////////////
