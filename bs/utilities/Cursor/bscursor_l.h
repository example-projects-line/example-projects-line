#ifndef __BSCURSOR_L_H__
#define __BSCURSOR_L_H__

#include "bscursorinfo.h"

#include <string>

class BSCursorPrivate
{

public:

     BSCursorPrivate();
    ~BSCursorPrivate();

     //	Gets the current cursor information.
     BSCursorInfo   getCurrentCursorInfo();

private:
    //  Maps the platform cursor ID with the correct BSCursor.
	int mapWithSystemCursor( std::string &systemCursorName );
    //  Fills in all the IDs of the supported cursors on a platform level.
    void    fillSupportedSysCursors();

private:
    //  Holds all the IDs of the supported cursors on a platform level.
    //  IMPORTANT: all platform cursors MUST be filled in the same order as in the
    //  BSCursorImages enumerate.
    //	NOTE: On Linux platform we cannot compare the ID of the current cursor
    //	with the 'HANDLE' of a supported cursor that has been previously loaded.
    //	We can get only the name it was created with.
    //	That's why when we compare the current cursor with all our supported ones,
    //	we compare them by names.
    std::string*	    m_systemCursorNamesGNOME;
    std::string*	    m_systemCursorNamesKDE;
    BSCursorImages	    m_cursorImageIds;
};

#endif // __BSCURSOR_L_H__
