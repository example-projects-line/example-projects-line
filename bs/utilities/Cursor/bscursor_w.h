#ifndef __BSCURSOR_W_H__
#define __BSCURSOR_W_H__

#include "bscursorinfo.h"

class BSCursorPrivate
{

public:

	 BSCursorPrivate();
	~BSCursorPrivate();

	 int*	getCursors();

	 //	Gets the current cursor information.
	 BSCursorInfo	getCurrentCursorInfo();

private:
	//	Maps the platform cursor ID with the correct BSCursor.
	int				mapWithSystemCursor( int systemCursorId );
	//	Fills in all the IDs of the supported cursors on a platform level.
	void			fillSupportedSysCursors();

private:
	//	Holds all the IDs of the supported cursors on a platform level.
	//	IMPORTANT: all platform cursors MUST be filled in the same order as in the
	//	BSCursorImages enumerate.
	int*			m_systemCursorIds;
	BSCursorImages	m_cursorImageIds;
};
#endif // __BSCURSOR_W_H__
