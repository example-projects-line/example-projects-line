////////////////////////////////////////////////////////////////////////////////

#include "bscursor_w.h"

#include "Windows.h"
#include <iostream>

////////////////////////////////////////////////////////////////////////////////

const int NUMBER_OF_SUPPORTED_CURSORS	= 11;

////////////////////////////////////////////////////////////////////////////////

BSCursorPrivate::BSCursorPrivate() : m_systemCursorIds( NULL )
{
	//	Allocate the necessary memory.
	m_systemCursorIds	= new int[ NUMBER_OF_SUPPORTED_CURSORS ];
	//	Fills in the supported system cursors' IDs.
	this->fillSupportedSysCursors();
}
////////////////////////////////////////////////////////////////////////////////

BSCursorPrivate::~BSCursorPrivate()
{
	//	Free used memory.
	if( m_systemCursorIds != NULL )
	{
		delete[]	m_systemCursorIds;
		m_systemCursorIds	= NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	BSCursorPrivate::fillSupportedSysCursors()
{
	m_systemCursorIds[0]	= (UINT32)::LoadCursor( NULL, IDC_ARROW			);
	m_systemCursorIds[1]	= (UINT32)::LoadCursor( NULL, IDC_APPSTARTING	);
	m_systemCursorIds[2]	= (UINT32)::LoadCursor( NULL, IDC_WAIT			);
	m_systemCursorIds[3]	= (UINT32)::LoadCursor( NULL, IDC_IBEAM			);
	m_systemCursorIds[4]	= (UINT32)::LoadCursor( NULL, IDC_SIZENS		);
	m_systemCursorIds[5]	= (UINT32)::LoadCursor( NULL, IDC_SIZEWE		);
	m_systemCursorIds[6]	= (UINT32)::LoadCursor( NULL, IDC_SIZENWSE		);
	m_systemCursorIds[7]	= (UINT32)::LoadCursor( NULL, IDC_SIZENESW		);
	m_systemCursorIds[8]	= (UINT32)::LoadCursor( NULL, IDC_SIZEALL		);
	m_systemCursorIds[9]	= (UINT32)::LoadCursor( NULL, IDC_HAND			);
	//	On X11 systems we have two cursors that look like an IBEAM.
	m_systemCursorIds[10]	= (UINT32)::LoadCursor( NULL, IDC_IBEAM			);
}
////////////////////////////////////////////////////////////////////////////////

int	BSCursorPrivate::mapWithSystemCursor( int systemCursorId )
{
	/*
		Default value is 0, cause if we capture unsupported cursor,
		we show the default arrow cursor.
	*/
	int	value	= 0;

	/*
		Loop through all the supported system cursors' IDs
		to find its index in the BSCursorImages struct.
	*/
	for( int i = 0; i < NUMBER_OF_SUPPORTED_CURSORS; i++ )
	{

		if( systemCursorId	== m_systemCursorIds[i] )
			return i;
	}

	return value;
}
////////////////////////////////////////////////////////////////////////////////

BSCursorInfo	BSCursorPrivate::getCurrentCursorInfo()
{
	BSCursorInfo	bsInfo;

	CURSORINFO  cursorInfo;

	UINT32	cursorID;

	cursorInfo.cbSize	= sizeof( CURSORINFO );
	GetCursorInfo( &cursorInfo );

	if( cursorInfo.flags    == CURSOR_SHOWING )	//  Cursor showing
	{
		cursorID	=  (UINT32)cursorInfo.hCursor;
	}

	bsInfo.cursorImgID	= (BSCursorImages)mapWithSystemCursor( cursorID );
	bsInfo.cursorPosX	= cursorInfo.ptScreenPos.x;
	bsInfo.cursorPosY	= cursorInfo.ptScreenPos.y;

	return bsInfo;
}
////////////////////////////////////////////////////////////////////////////////

int*	BSCursorPrivate::getCursors()
{
	return m_systemCursorIds;
}
////////////////////////////////////////////////////////////////////////////////
