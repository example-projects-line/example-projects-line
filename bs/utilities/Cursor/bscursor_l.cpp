#include "bscursor_l.h"

#include <X11/Xlib.h>
#include <X11/cursorfont.h>
#include <X11/extensions/Xfixes.h>

#include <iostream>

////////////////////////////////////////////////////////////////////////////////

const int NUMBER_OF_SUPPORTED_CURSORS	= 11;

////////////////////////////////////////////////////////////////////////////////

BSCursorPrivate::BSCursorPrivate() : m_systemCursorNamesGNOME( NULL ),
									 m_systemCursorNamesKDE	 ( NULL )
{
	//	Allocate the necessary memory and
	//	fill in the supported system cursors' names.
	m_systemCursorNamesGNOME	= new std::string[NUMBER_OF_SUPPORTED_CURSORS];
	m_systemCursorNamesKDE		= new std::string[NUMBER_OF_SUPPORTED_CURSORS];

	this->fillSupportedSysCursors();
}
////////////////////////////////////////////////////////////////////////////////

BSCursorPrivate::~BSCursorPrivate()
{
	//	Free used memory.
	if( m_systemCursorNamesGNOME != NULL )
	{
		delete[]	m_systemCursorNamesGNOME;
		m_systemCursorNamesGNOME	= NULL;
	}

	if( m_systemCursorNamesKDE != NULL )
	{
		delete[]	m_systemCursorNamesKDE;
		m_systemCursorNamesKDE		= NULL;
	}
}
////////////////////////////////////////////////////////////////////////////////

void	BSCursorPrivate::fillSupportedSysCursors()
{
    //	Fill in GNOME supported cursor names.
	m_systemCursorNamesGNOME[0]		= "left_ptr";
	m_systemCursorNamesGNOME[1]		= "left_ptr";	//	X11 system do not have app starting cursor.
	m_systemCursorNamesGNOME[2]		= "left_ptr";	//	On x11 system we are not able to capture the waiting cursor.
	m_systemCursorNamesGNOME[3]		= "ibeam";
	m_systemCursorNamesGNOME[4]		= "top_side";
	m_systemCursorNamesGNOME[5]		= "right_side";
	m_systemCursorNamesGNOME[6]		= "top_right_corner";
	m_systemCursorNamesGNOME[7]		= "top_left_corner";
	m_systemCursorNamesGNOME[8]		= "fleur";
	m_systemCursorNamesGNOME[9]		= "hand2";
	m_systemCursorNamesGNOME[10]	= "xterm";

    //	Fill in KDE supported cursor names.
	m_systemCursorNamesKDE[0]		= "left_ptr";
	m_systemCursorNamesKDE[1]		= "left_ptr";	//	X11 system do not have app starting cursor.
	m_systemCursorNamesKDE[2]		= "left_ptr";	//	On x11 system we are not able to capture the waiting cursor.
	m_systemCursorNamesKDE[3]		= "ibeam";
	m_systemCursorNamesKDE[4]		= "size_ver";
	m_systemCursorNamesKDE[5]		= "size_hor";
	m_systemCursorNamesKDE[6]		= "size_bdiag";
	m_systemCursorNamesKDE[7]		= "size_fdiag";
	m_systemCursorNamesKDE[8]		= "fleur";
	m_systemCursorNamesKDE[9]		= "hand2";
	m_systemCursorNamesKDE[10]		= "xterm";
}
////////////////////////////////////////////////////////////////////////////////

int	BSCursorPrivate::mapWithSystemCursor( std::string &systemCursorId )
{
	/*
		Default value is 0, cause if we capture unsupported cursor,
		we show the default arrow cursor.
	*/
	int	value	= -1;

	/*
		Loop through all the supported system cursors' IDs
		to find its index in the BSCursorImages struct.
	*/
	for( int i = 0; i < NUMBER_OF_SUPPORTED_CURSORS; i++ )
	{

		if( systemCursorId == m_systemCursorNamesKDE[i] || systemCursorId == m_systemCursorNamesGNOME[i] )
			return i;
	}

	return value;
}
////////////////////////////////////////////////////////////////////////////////

BSCursorInfo	BSCursorPrivate::getCurrentCursorInfo()
{
	BSCursorInfo	bsInfo;

	Display*			dpy				= XOpenDisplay( NULL );
	XFixesCursorImage*  cursor_image    = XFixesGetCursorImage ( dpy );

	std::string cursorName				= cursor_image->name;

	bsInfo.cursorImgID		    = (BSCursorImages)mapWithSystemCursor( cursorName );
	bsInfo.cursorPosX		    = cursor_image->x;
	bsInfo.cursorPosY		    = cursor_image->y;

	return bsInfo;
}
////////////////////////////////////////////////////////////////////////////////
