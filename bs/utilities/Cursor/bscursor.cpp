#include "bscursor.h"

#ifdef PLATFORM_WIN32
#include "bscursor_w.h"
#endif
#ifdef PLATFORM_LINUX
#include "bscursor_l.h"
#endif
#ifdef PLATFORM_MAC
#include "bscursor_m.h"
#endif

////////////////////////////////////////////////////////////////////////////////

BSCursor::BSCursor()
{
	m_d		= new BSCursorPrivate();
}
////////////////////////////////////////////////////////////////////////////////

BSCursor::~BSCursor()
{
	delete m_d;
}
////////////////////////////////////////////////////////////////////////////////

void	BSCursor::loadCursorInfo()
{
	BSCursorInfo	info	= m_d->getCurrentCursorInfo();

	m_x                     = info.cursorPosX;
	m_y                     = info.cursorPosY;
	m_image					= info.cursorImgID;
}
////////////////////////////////////////////////////////////////////////////////
