#ifndef __BSCURSORINFO_H__
#define __BSCURSORINFO_H__

enum BSCursorImages
{
	CURSOR_NORMAL_SELECT		= 0,
	CURSOR_WORKING_IN_BACKGR	= 1,
	CURSOR_BUSY					= 2,
	CURSOR_TEXT_SELECT			= 3,
	CURSOR_UNAVAILABLE			= 4,
	CURSOR_VERTICAL_RESIZE		= 5,
	CURSOR_HORIZONTAL_RESIZE	= 6,
	CURSOR_DIAGONAL_RESIZE_SW	= 7,
	CURSOR_DIAGONAL_RESIZE_SE	= 8,
	CURSOR_MOVE					= 9,
	CURSOR_LINK_SELECT			= 10

};

typedef	struct BSCursorInfo
{
	int				cursorPosX;
	int				cursorPosY;
	BSCursorImages	cursorImgID;

}BSCursorInfo;


#endif // __BSCURSORINFO_H__
