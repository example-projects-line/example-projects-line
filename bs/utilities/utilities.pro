TEMPLATE = lib
TARGET	 = utilities

CONFIG	+= staticlib x86 debug
#CONFIG	+= staticlib x86 release

win32{
HEADERS =	Cursor/bscursor.h \
			Cursor/bscursorinfo.h\
			Cursor/bscursor_w.h \
SOURCES =	Cursor/bscursor.cpp\
			Cursor/bscursor_w.cpp \

DEFINES =  PLATFORM_WIN32

}

linux-g++{
HEADERS +=  Cursor/bscursor.h \
			Cursor/bscursorinfo.h\
			Cursor/bscursor_l.h
SOURCES +=  Cursor/bscursor.cpp\
			Cursor/bscursor_l.cpp
DEFINES +=  PLATFORM_LINUX

DESTDIR  = ~/projects/bs/utilities/bin/

}
