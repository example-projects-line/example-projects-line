#-------------------------------------------------
#
# Project created by QtCreator 2011-11-06T12:58:03
#
#-------------------------------------------------

QT          += core gui

TARGET      = bsdeling
TEMPLATE    = app

CONFIG      += x86 qt debug warn_off
#CONFIG		+= x86 qt release

SOURCES += main.cpp\
                mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

win32{
LIBS	= ../utilities/debug/utilities.lib
}
linux-g++{
LIBS	+= -lutilities -L../utilities/bin/ -lX11 -lXfixes -lQtGui -lQtCore -lpthread
}
