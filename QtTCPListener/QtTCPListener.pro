#-------------------------------------------------
#
# Project created by QtCreator 2012-02-15T20:59:56
#
#-------------------------------------------------

QT       += core gui

TARGET = QtTCPListener
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

QT += network
