
/*****************************************************************************
FILE		: mainwindow.cpp
DESCRIPTION	: 

*****************************************************************************

Copyright (C) 2012 Svetlomir Zhelev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow( QWidget *parent ) :
    QMainWindow( parent ),
    m_size( -1 ),
    m_tcpServer( NULL ),
    m_message( NULL ),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_tcpServer	= new QTcpServer( this );
    m_message	= new QByteArray();

    setWindowState( Qt::WindowMaximized );
}

MainWindow::~MainWindow()
{
    delete ui;
}
////////////////////////////////////////////////////////////////////////////////

void MainWindow::on_pushButton_clicked()
{
    qint16  port    = (qint16)ui->lineEdit->text().toInt();
    m_tcpServer->listen( QHostAddress("127.0.0.1"), port );

    if( m_tcpServer->isListening() )
    {
	qDebug() << "Stasted listening on port : " << m_tcpServer->serverPort();
	connect( m_tcpServer, SIGNAL(newConnection()), this, SLOT(sendResponse()) );
	ui->pushButton->setEnabled( false );
    }

}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::sendResponse()
{
    m_client	= m_tcpServer->nextPendingConnection();
    connect( m_client, SIGNAL(readyRead()), SLOT(onReadyRead()) );
}
////////////////////////////////////////////////////////////////////////////////

void	MainWindow::onReadyRead()
{
    QDataStream in( m_client );
    in.setVersion(QDataStream::Qt_4_0);

    if( m_size == -1 )
    {
	int blockSize;
	in >> blockSize;

	qWarning() << "Total bytes count : " << blockSize;

	m_size = blockSize;
    }
    if( m_size != -1 )
    {
	    QByteArray message;
	    message = m_client->readAll();
	    m_message->append( message );

	    qWarning() << "m_message size is :" << m_message->size();

	if( m_size == m_message->size() )
	{
	    QImage img	= QImage::fromData( *m_message );
	    QPixmap pixmap  = QPixmap::fromImage( img );

	    ui->m_lblStatus->resize( QSize(img.width(), img.height()) );
	    ui->m_lblStatus->setPixmap( pixmap );
	}
    }
}
////////////////////////////////////////////////////////////////////////////////
